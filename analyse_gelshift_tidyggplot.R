###################################
# gel shift analysis quantification 
###################################

# run by source("/home/yann/Documents/TP63_ARTICLE/DATA/BINDING_DATA/analyse_gelshift_tidyggplot.R")
# quantification of Cy3 signal obtained from typhoon scanner are performed as follow
# open image with bioformat-importer
# invert image, invert LUT to have dark signal with higher values and light signals with lower
# draw rectangle for free, CTRL M for each lane
# draw rectangle for bound, CTRL M for each lane
# draw one rectangle for background  data are "quantif_11032021"
# 25112020/23112020/20112020/17112020

# format of the input file
#col	area	mean	min	max	complex	prot_conc	RNA	date

# libraries
  library(tidyverse)
  library(broom)

#indir="/homedir/UR1/yaudic/Documents/THESE_WILLIAM/Science/p63_binding sites/RNA_SHIFT/EMSA_results/FIGURES"
  indir="/home/yann/Documents/TP63_ARTICLE/DATA/BINDING_DATA"
  infile="quantif_11032021"
# protein concentration based on comparison with coomassie staining of BSA 
  prot_conc=c(0,2.7,8,25,74,222,667,NA) 

#dataset=read.csv(file.path(indir,infile),sep="\t",dec=".",comment.char="#",header=TRUE)
  dataset.tbl=read_tsv(file.path(indir,infile),comment="#")
#set prot conc to factor 
  dataset.tbl$prot_conc=as.factor(dataset.tbl$prot_conc)
#convert the levels of protein concentration to values (still levels)
  dataset.tbl$prot_conc <- recode(dataset.tbl$prot_conc,"0"="0","1"="2.7","2"="8","3"="25","4"="74","5"="222","6"="667")

# for each category defined by RNA and date compute the AVGsignal-AVG_BCKGRD

# convert dataframe to tibble
#dataset.tbl=as_tibble(dataset)
#convert the levels of protein concentration to values (still levels)
#levels(dataset$prot_conc)=prot_conc
	dataset.tbl$prot_conc=as.numeric(as.character(dataset.tbl$prot_conc))
# normalised the data by substracting the mean background value and compute volume
	dataset.tbl %>%  group_by(RNA,date)  %>% mutate(MEAN_Bgd=mean - mean[complex=="BCKGRD"]) %>% ungroup -> normalised_dataset.tbl
# if the normalised value is below 0 then it is set to 0
	normalised_dataset.tbl[normalised_dataset.tbl$MEAN_Bgd <0,"MEAN_Bgd"]=0

#compute the volume
	normalised_dataset.tbl %>% ungroup %>% mutate(volume=MEAN_Bgd * area) -> normalised_dataset.tbl

# ok now normalised_dataset.tbl contains the volume info

#filter(flights, month == 1)
# compute the bound/bound+free signal for complex different from background
	normalised_dataset.tbl %>% filter (complex != "BCKGRD") %>% group_by(RNA,date,prot_conc) %>% mutate(B_BplusF=volume[complex=="bound"] /(volume[complex=="bound"]+volume[complex=="free"])) %>% ungroup-> computed_values.tbl

# keep only info for free 
test = computed_values.tbl %>% filter(complex=="free") 
#test_fragt6 = test %>% filter(RNA=="FRAGT6")


# plot complex formatino vs PTBP1 concentration and fit with formula
ggplot(test,aes(x=prot_conc, y=B_BplusF)) +  geom_point() + facet_wrap(~RNA) + geom_smooth(aes(group=RNA),method="nls", formula=y ~ 1/(1 + Kd/x), method.args = list(start=c(Kd=100)), se=FALSE)


# graph of all the plots formula=y ~ 1/(1 + Kd/x)
pdf(file=file.path(indir,"analysis_EMSA_TO_10032021simple.pdf"),width=14,height=14)
p <- ggplot(test,aes(x=prot_conc, y=B_BplusF)) +  geom_point() + facet_wrap(~RNA) + geom_smooth(aes(group=RNA),method="nls", formula=y ~ 1/(1 + Kd/x), method.args = list(start=c(Kd=100)), se=FALSE)
p +  ggtitle("EMSA with hsPTBP1 \n fitted with formula=y ~ 1/(1 + Kd/x)") +  xlab("hsPtbp1 (nM)") + ylab("bound/bound+free")
dev.off()

# graph for all the plots but the mut4
# this mutant sequence partially binds PTBP1 in UV cross link experiments it can not be used as a negative control
	test_notmut4 = test %>% filter(RNA!="150PYMUT4")
pdf(file=file.path(indir,"analysis_EMSA_TO_10032021simple_final.pdf"),width=14,height=14)
	test <- test_notmut4
	p <- ggplot(test,aes(x=prot_conc, y=B_BplusF)) +  geom_point() + facet_wrap(~RNA) + geom_smooth(aes(group=RNA),method="nls", formula=y ~ 1/(1 + Kd/x), method.args = list(start=c(Kd=100)), se=FALSE)
	p +  ggtitle("EMSA with hsPTBP1 \n fitted with formula=y ~ 1/(1 + Kd/x)") +  xlab("hsPtbp1 (nM)") + ylab("bound/bound+free")
dev.off()



# get the equation parameter and standard error from the curves https://douglas-watson.github.io/post/2018-09_dplyr_curve_fitting/
test %>% nest (data=c(-RNA)) %>% mutate(fit = map(data, ~ nls(formula=B_BplusF ~ 1/(1 + Kd/prot_conc),start=c(Kd=100), data = .)), tidied = map(fit, tidy)) %>%  unnest(tidied) %>% select(RNA, term, estimate, std.error) %>% pivot_wider(names_from ="term", values_from=estimate)

#   RNA       std.error      Kd
#   <chr>         <dbl>   <dbl>
# 1 FRAGT6         5.00    61.1
# 2 FRAGT5       108.    1026. 
# 3 FRAGT1       623.    2441. 
# 4 FRAGT2       702.    4248. 
# 5 150PYWT        9.20   104. 
# 6 150PYMUT4     24.0    138. 
# 7 FRAGT4        23.4    151. 
# 8 FRAGT3        12.8    147. 
# 9 FRAGT7      5638.   15622. 
#10 CPA2           8.16    60.5
#11 CPA1         256.     777. 

########################################################################
# graph of all the plots formula=y ~  b+((m-b)/(1 + (Kd/x)^n)
#pdf(file=file.path(indir,"analysis_EMSA_to_11032021_complex.pdf"),width=14,height=14)
#p<- ggplot(test,aes(x=prot_conc, y=B_BplusF)) +  geom_point() + facet_wrap(~RNA) + geom_smooth(aes(group=RNA),method="nls", formula=y ~  b+((m-b)/(1 + (Kd/x)^n)), method.args = list(start = c(Kd=500,n=2,m=1,b=0)),se=FALSE)
#p +  ggtitle("EMSA with hsPTBP1 150PyWT and 150Pymut4 \n fitted with formula=y ~  b+((m-b)/(1 + (Kd/x)^n))") +
#  xlab("hsPtbp1 (nM)") + ylab("bound/bound+free")
#dev.off()

# OK RUNNING SMOOTHLY

