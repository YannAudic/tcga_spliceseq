# TP63/PTBP1 manuscript gitlab for R script

This project host the R scripts initially associated to the manuscript Taylor et al.
The aim of this project is to clean the script and to propose a R Package with 
a) easy usage
b) possibility to systematically analyse the association between patient survival and alternative splicing in the tumor samples.

1) Analysis of RNA splicing for TP63 gene in TCGA HNSCC data and survival analysis
"TCGA_TP63_taylor01092021.R"

The example files included in this gitlab correspond to :
 1) HNSC clinical data from TCGA
 2) normalised gene expression data from TCGA
 3) Percent splcied in data from SPLICESEQ

 - The goal is to produce a functional R package that take advantages of the the TCGA_TP63_taylor01092021.R script already produced following the following procedure

```mermaid
graph TD;
Firebrowse-->TCGA_geneexp;
Firebrowse-->TCGA_clinical;
SpliceSeq-->TCGA_splicing_data ;
TCGA_geneexp-->Get_genes_expr_tumor;
TCGA_splicing_data-->Get_splicedataforgenes_expr_tumor
Get_genes_expr_tumor-->Get_splicedataforgenes_expr_tumor;
TCGA_clinical-->test_survivalforallsplicedata;
Get_splicedataforgenes_expr_tumor-->test_survivalforallsplicedata;
test_survivalforallsplicedata --> RESULTS

```




(https://docs.gitlab.com/ee/gitlab-basics/create-project.html)
